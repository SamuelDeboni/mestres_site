---
title: "Só desenhar uma linha"
date: 2021-07-06T10:38:19-03:00
author: "Samuel Deboni Fraga"
draft: false
---

Voce já parou para pensar em como um computador faz algo simples como desenhar uma linha? Parece ser algo simples, e é simples, porém tem muita coisa envolvendo a solução de um problema simples como esse.

O objetivo desse post não é o de ensinar um algoritmo específico, mas sim mostrar como se soluciona um problema de programação. Eu vou estar mostrando passo a passo como criar um algoritmo simples de desenhar uma linha.


## Definindo o problema

A primeira etapa na solução de qualquer problema é você entender o problema que precisa ser solucionado.

*Mas já sabemos, precisamos desenhar uma linha*

Sim, porém existem várias formas de desenhar uma linha: vamos desenhar uma linha infinita? Como vamos representar a linha? a linha vai ter só uma cor? vai ter espessura variável? vai ter anti-aliasing? Como queremos representar isso em código?

#### Pensar no resultado

A forma mais comum de representar uma linha em uma biblioteca gráfica é usando 2 pontos. 

![](graph.png)

Então precisamos descobrir como vamos pegar essas informações e gerar o resultado desejado.

#### Saber como queremos o código faz parte do problema

Nós queremos desenhar uma linha usando o computador, então também é uma boa ideia pensar em como queremos que a solução seja usada. Para fazer isso escreva alguns casos de uso.

```c
/*
 Eu escolhi fazer uma função simples,
 Temos que passar algum buffer para ser desenhado,
 as coordenadas dos dois pontos que representam uma linha
 e a cor da linha
*/
draw_line(canvas, x0, y0, x1, y1, color);
```

## Planejar a solução

Após entender o problema, precisamos pensar em como ele vai ser solucionado.

*Mas onde começar?*

Nós queremos solucionar o problema usando um algoritmo, mas oque é um algoritmo?

Segundo o Wikipedia, um algoritmo é uma sequência finita de ações executáveis que visam obter uma solução para um determinado tipo de problema.

Porém existe uma outra definição que eu acho mais útil.

**Um algoritmo é uma sequência de etapas com o objetivo de transformar dados.**

Se um algoritmo é transformar dados, precisamos pensar nos dados de entrada e nos de saída. As entradas são fáceis, 2 pontos e 1 cor. Já a saída pode ser pensada em quais pixels precisamos pintar para desenhar a linha.


Vamos colocar uma linha em cima de uma grid de pixels.
![](line_grid_no_fill.png)

E pintar os pixels.
![](line_grid_fill.png)

Nós já sabemos o estado inicial e final dos dados, agora é que começa a parte interessante, como transformar esses dados.

Nós sabemos 2 pixeis que precisam ser coloridos, o inicial e o final, mas e o meio?

Nós temos muito mais informações do que parece. Um ponto é formado de 2 componentes, a posição **x** e a **y**, e se reparar no desenho a linha podemos perceber que para todas as colunas entre os 2 pontos, somente tem 1 pixel colorido, podemos usar isso para saber quais pixels precisamos pintar.

![](line_grid2.png)

Nós sabemos a posição **x**, todas entre os 2 pontos, agora precisamos descobrir a **y**.

Vamos olhar para a definição matemática de uma linha:
![](line_definition.png)

Nós temos o **x**, então agora nos precisamos descobrir o **a** e o **b**.

O **a** representa a inclinação da linha e o **b** representa a posição.

No nosso caso podemos ignorar o **b**(Isso vai ficar mais claro mais a frente), e para achar o **a** podemos usar a seguinte fórmula:
![](a_definition.png)

- Eu não vou explicar como cheguei nessa fórmula porque já sai do foco desse post, talvez no futuro eu escreva um post sobre como achar esse tipo de fórmula.

Então agora já sabemos como desenhar, precisamos fazer um código que para cada posição em **x**, calculamos a posição e desenhamos na tela.

## O Código

Agora vamos para o que interessa, o código.


Começamos com a definição da função, o `Canvas` é simplesmente uma biblioteca que estou usando para desenhar os pixeis, e a função recebe 2 pontos e uma cor.


```c++
draw_line :: proc(using canvas: Canvas, point_0: [2]int, point_1: [2]int, color: u32) {

}

```

Como nesse metodos precisamos trabalhar com pontos flutuantes, vou dar um cast para `p0` e `p1`
```c++
p0 := [2]f32{f32(point_0.x), f32(point_0.y)};
p1 := [2]f32{f32(point_1.x), f32(point_1.y)};

```


Calculamos o `a`, e criamos o loop em todos os valores de `x`, calculando o `y`, pintando os pixels.
```c++
// ...

a := (p1.y - p0.y) / (p1.x - p0.x);
y := p0.y;

for x := p0.x; x <= p1.x; x += 1 {
    put_pixel(canvas, x, y, color);
    y += a;
}

```

Vamos ver o resultado

![](line_result_0.png)

Porém temos um problema, se o tamanho da linha em x for menor do que o em y não é renderizado corretamente

![](line_result_1.png)

Isso ocorre porque iteramos no x e achamos o y, para todo o x temos no máximo um pixel em y, porém para cada y temos mais de um pixel em x.

Existem algumas formas de solucionar esse problema, um jeito seria achar a diferença entre o y do pixel atual e do próximo e desenhar a quantidade de pixeis necessárias. Porém existe um jeito mais fácil e mais rápido, nós vamos simplesmente verificar se o tamanho em x é maior ou menor do que o em y, se o em x for maior iteramos no x, se o em y for maior iteramos no y e achamos o x.

```c++
if abs(p1.x - p0.x) > abs(p1.y - p0.y) {
    a := (p1.y - p0.y) / (p1.x - p0.x);
    y := p0.y;
    
    for x := p0.x; x <= p1.x; x += 1 {
        put_pixel(canvas, x, y, color);
        y += delta;
    }
    
} else {    
    a := (p1.x - p0.x) / (p1.y - p0.y);
    x := p0.x;
    
    for y := p0.y; y <= p1.y; y += 1 {
        put_pixel(canvas, x, y, color);
        x += delta;
    }
}
```

Agora a linha é renderizada corretamente

![](line_result_2.png)

## Ainda temos alguns problemas

Eu mostrei, ou tentei mostrar, o meu raciocínio ao criar esse algoritmo. Eu não mostrei todo o processo, mas sim tentei mostrar a minha ideia central ao solucionar um problema desse. Porém como eu não mostrei tudo ainda existem alguns problemas nesse algoritmo.

Caso o x1 for menor que o x0 ou se o y1 for menor que o y0 a linha não é renderizada pois nós incrementamos o x e y. A forma que eu solucionei foi garantir que o x0 é menor caso iterar no x, e o y0 é menor caso iterar no y. 

Também temos que verificar se os pontos estão fora da tela. Essa verificação pode ser feita em 2 partes, antes do loop fazendo clamping dos pontos, ou dentro do looping só desenhando caso esteja dentro. 

Porém, se eu conseguir meu objetivo com esse post você deve ser capaz de achar essa solução por você mesmo.

*Eu não estou com preguiça de escrever o resto*

## O código pronto

Para aqueles que precisam, ou somente estão curiosos, aqui é a função completa.

```c++
draw_line :: proc(using canvas: Canvas, point_0: [2]int, point_1: [2]int, color: u32) {
    // NOTE(samuel): Verify if the line is outside of the screen
    if  point_0.x < 0 && point_1.x < 0 ||
        point_0.y < 0 && point_1.y < 0 ||
        point_0.x >= int(width)  && point_1.x >= int(width) ||
        point_0.y >= int(height) && point_1.y >= int(height)
    {
        return;
    }
   
    swap_if_greater :: proc(a: ^[2]f32, b: ^[2]f32, c: int) {
        if a[c] > b[c] {
            tmp := a^;
            a^ = b^;
            b^ = tmp;
        }
    }
    
    p0 := [2]f32{f32(point_0.x), f32(point_0.y)};
    p1 := [2]f32{f32(point_1.x), f32(point_1.y)};
    
    // NOTE(samuel): Clamp the points
    {
        swap_if_greater(&p0, &p1, 0);
        
        a := (p1.y - p0.y) / (p1.x - p0.x);
        if p0.x < 0 {
            x_delta := -p0.x;
            p0.x = 0;
            p0.y += x_delta * a;
        }
        
        if p1.x >= f32(width) {
            x_delta := f32(width) - p1.x;
            p1.x = f32(width - 1);
            p1.y += x_delta * a;
        }
        
        swap_if_greater(&p0, &p1, 1);
        
        a = (p1.x - p0.x) / (p1.y - p0.y);
        if p0.y < 0 {
            y_delta := -p0.y;
            p0.x += y_delta * a;
            p0.y = 0;
        }
        
        if p1.y >= f32(height) {
            y_delta := f32(height) - p1.y;
            p1.x += y_delta * a;
            p1.y = f32(height - 1);
        }
    }

    // NOTE(samuel): Drawing loop  
    if abs(p1.x - p0.x) > abs(p1.y - p0.y) {
        swap_if_greater(&p0, &p1, 0);
        
        delta := (p1.y - p0.y) / (p1.x - p0.x);
        y := p0.y;
        
        for x := p0.x; x <= p1.x; x += 1 {
            put_pixel(canvas, x, y, color);
            y += delta;
        }
        
    } else {
        swap_if_greater(&p0, &p1, 1);
        
        delta := (p1.x - p0.x) / (p1.y - p0.y);
        x := p0.x;
        
        for y := p0.y; y <= p1.y; y += 1 {
            put_pixel(canvas, x, y, color);
            x += delta;
        }
    }
}

```