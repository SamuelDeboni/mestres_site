---
title: "Syscalls. Onde vivem? O que comem?"
date: 2021-07-06T10:38:19-03:00
draft: true
---

## Introdução

Olá pessoas (e bots de webscrap), hoje eu gostaria de falar acerca de syscalls, e de como elas são importantes e de do quanto vale a pena estudar-las, mesmo que não aprofundemos em visitar todas as syscalls que o Linux apresenta, que é o sistema que irei focar neste tutorial (mas não se desepere se você usa o Windão).

Primeiramente, para entender o que é uma syscall, devemos entender alguns fundamentos básicos sobre sistemas operacionais, pelo menos no tocante à maioria dos sistemas operacionais modernos.

É função do sistema operacional administrar, mais especificamente os recursos de um computador, na qual podemos citar: memória RAM, armazenamento, network, entre outros; e criar um ambiente onde diversos processos possam solicitar e utilizar estes recursos, de maneira fácil de ágil. O sistema operacional deve lidar com diversas situações de estresse como: diversos processos solicitando pelo mesmo recursos ao mesmo tempo, a escasses de algum recurso. Usar um sistema operacional é um trato, eu troco a acesso direto e deliberado à um recurso da máquina, por ambiente de acesso limitado aos recursos, mas que eu não precise como desenvolvedor conhecer os aspectos específicos da máquina para acessar certos recursos. Por exemplo, eu troco a possibilidade de poder acessar diretamente e deliberadamente o disco rígido, por ambiente onde eu tenha um acesso mais limitado ao mesmo, entretanto, utilizando um sistema operacional eu não preciso, no caso de um disco rígido, me preocupar com posição do cabeçote, com setores e sistemas de arquivos. Antigamente, quando os recursos eram muito mais limitados do que são hoje, valia a pena criar um programa que rode diretamente sobre à máquina, visto que, o acesso direto e deliberado poderia ser a diferença entre meu programa ser usável ou não. Para a maioria dos problemas atuais este não é o caso, além do mais as plataformas modernas são tão complexas que construir uma aplicação que rode diretamente sobre uma máquina (sem a existência de um SO) pode ser uma grande empreitada.

Visto isso, as syscalls surgem como uma interface entre o processo e o sistema operacional. Por meio de uma instrução especial, o processo que deseja obter algum recurso específico do SO, seta um conjunto de registradores, e realiza uma chamada de sistema (por meio da execução desta instrução especial). Após isso o controle da CPU é passado ao sistema operacional, que vai processar a requisição e emitir uma resposta ao processo (isso pode ou não ocorrer, depende do sistema operacional e se o tipo da chamada de sistema retorna algo).

## Botando a mão na massa
