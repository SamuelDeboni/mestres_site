---
title: "Algoritmos Genéticos #0"
date: 2021-07-06T10:38:19-03:00
draft: true
---

Alguns problemas na computação não tem como ser resolvidos antes de justamente
se ter o problema. Algoritmos de ordenação, por exemplo, têm soluções empíricas.
No entanto, muitas vezes o problema não é tão "simples".

## Computação bioinspirada

Observando a natureza podemos ter ideias de como solucionar diversos problemas.
As redes neurais, por exemplo, são um tópico muito amplo, mas que têm
fundamentação ao observar como o cérebro dos animais funciona. Outra classe de
algoritmos desenvolvido a partir da observação das espécies são os Algoritmos
Genéticos.

## Algoritmo Genético

A ideia por trás do algoritmo genético é simples: temos uma população inicial,
diversificada, e essa população gera novos indivíduos. A taxa com a qual um
indivíduo da população inicial espalha seus genes deve estar relacionada com
quão boa é a solução que aquele indivíduo apresenta.

### A solução e o espaço de busca

Já foi mencionada a questão de o que é um indivíduo, mas vamos detalhar o que é
a solução e como o gene está relacionado com a solução.

A solução é um termo muito genérico, e depende completamente da classe de
problemas que estamos tratando. O nosso problema pode ser encontrar o máximo de
uma função matemática (sim!) ou pode ser otimizar o comportamento de um robô
móvel para otimizar seus parâmetros.

Vamos nos ater, por enquanto, ao caso da função matemática. Seja uma função
f(x) uma função que queremos maximizar. Utilizando um caso
simples, vamos definir ela como:

```
f(x) = -x, se x < 0
f(x) = x, se x >= 0
```

Obtemos aqui o seguinte gráfico:

![](./simple_func.png)

Para funções unidimensionais, no geral, nossa única variável é justamente a
variável de entrada da função. Então podemos estabelecer que os indivíduos da
nossa população são definidos pelo seguinte código:

```cpp
struct Individuo
{
	float X; // Nosso unico parametro, ou gene.
}
```

A variável `X` é o que vai ser utilizado para encontrar a solução. Resta agora
mais algumas definições.

### A função de finess

A função de finess no AG simplesmente define se a solução é boa ou não. No
exemplo que foi dado, a nossa própria `f` é a função de fitness. No geral, para
encontrar o máximo de qualquer função matemática, podemos criar esse sistema.

Vamos ver como seria uma função mais complexa de fitness:

![](./complex_func.png)

É possível calcular o máximo dessa função através de derivação, mas utilizando
AG nós temos diversas oportunidades de explorar técnicas para a busca da
	solução.

### Passos do algorimo genético

Tendo esses conceitos em mente, vamos definir os passos do AG:

```c
gera_populacao(); // Populacao inicial, tipicamente o mais diversa possivel.
while (true)
{
	avalia_populacao(); // Aplica a função de fitness nos indivíduos.
	substitui_populacao(); // Gera uma nova população baseada na nota
}
```

Simples, não? Vamos explorar o que é o `substitui_populacao`.

### Técnicas de evolução

#### Elitismo

O melhor indivíduo tem filhos com todos os outros.




